import firebase from 'firebase'

const getCurrentUser = () => {
  return firebase.auth().currentUser
}

const requiresUser = (callback) => {
  return new Promise((resolve, reject) => {
    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      unsubscribe()
      if (user) {
        resolve(user)
        return
      }
      reject()
    })
  })
}

// const requiresUser = (callback) => {
//   const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
//     unsubscribe()
//     callback(user)
//   })
// }

export default {
  getCurrentUser,
  requiresUser
}
