import Vue from 'vue'
import router from './router'
import firebase from 'firebase'

import '!script!jquery/dist/jquery.min.js'
import '!script!semantic-ui-css/semantic.min.js'
import '!style!css!semantic-ui-css/semantic.min.css'
import App from './App'

firebase.initializeApp({
  apiKey: 'AIzaSyCo5SK1dScH8RJsavl89Drcyl6TS45V9Tg',
  authDomain: 'twitto-api.firebaseapp.com',
  databaseURL: 'https://twitto-api.firebaseio.com',
  storageBucket: 'twitto-api.appspot.com',
  messagingSenderId: '696298710178'
})

Vue.filter('upper', (value) => {
  if (typeof value === 'string') {
    return value.toUpperCase()
  }
  return value
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
